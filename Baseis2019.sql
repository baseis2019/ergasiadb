CREATE TABLE tickets (
  ticketid int UNIQUE PRIMARY KEY NOT NULL,
  type text,
  location text,
  price float,
  startdate date,
  enddate date,
  category text,
  companyid int
);

CREATE TABLE customers (
  customerid int UNIQUE PRIMARY KEY NOT NULL,
  firstname text,
  lastname text,
  customerafm text,
  cellphonenumber text,
  email text,
  creditcardnumber text,
  age int
);

CREATE TABLE transactions (
  transactionid int UNIQUE PRIMARY KEY NOT NULL,
  customerid int,
  ticketid int,
  dayoftransaction date
);

CREATE TABLE companies (
  companyid int UNIQUE PRIMARY KEY NOT NULL,
  companyname text,
  companyafm text
);

ALTER TABLE tickets ADD FOREIGN KEY (companyid) REFERENCES companies (companyid);

ALTER TABLE transactions ADD FOREIGN KEY (customerid) REFERENCES customers (customerid);

ALTER TABLE transactions ADD FOREIGN KEY (ticketid) REFERENCES tickets (ticketid);
