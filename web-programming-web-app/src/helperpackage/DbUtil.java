package helperpackage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbUtil {

    private static Connection connection = null;
    private static final String jdbUrl = "jdbc:postgresql://localhost:5432/Baseis2019";
    private static final String username = "postgres";
    private static final String password = "postgre";

    public static Connection getConnection() {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        if(connection == null) {
            try{
                connection = DriverManager.getConnection(jdbUrl, username, password);
            }
            catch (SQLException e){
                e.printStackTrace();
            }
        }

        return connection;
    }
}
