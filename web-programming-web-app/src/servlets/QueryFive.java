package servlets;

import helperpackage.DbUtil;
import helperpackage.PageBuilder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet("/QueryFive")
public class QueryFive extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private Connection connection = null;
    PageBuilder pgb = new PageBuilder();

    public void init() throws ServletException{
        try {
            connection = DbUtil.getConnection();
        } catch(Exception e) {
            throw new ServletException(e.toString());
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println(pgb.getDefaultHead("First Query"));
        out.println("<body><div align=\"center\">");

        try{
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT cat1 AS category,customerid, max FROM ((SELECT cat1, MAX(count) FROM\n" +
                                                "\t\t(SELECT category AS cat1, customerid, COUNT(ticketid) FROM\n" +
                                                "\t\t\ttransactions JOIN tickets USING(ticketid)\n" +
                                                "\t\t\tGROUP BY cat1, customerid ORDER BY count) AS customerTickets \n" +
                                                "\t\t\tGROUP BY cat1) maxTickets\n JOIN \n" +
                                                "\t\t(SELECT category AS cat2, customerid, COUNT(ticketid) FROM\n" +
                                                "\t\t\ttransactions JOIN tickets USING(ticketid) GROUP BY cat2, customerid) customerTickets ON count=max AND cat1=cat2)\n" +
                                                "AS results ORDER BY category, customerid;");
            out.println("<table class=\"table-dark table-hover table-bordered\"><tr><th>Category</th><th>Customer ID</th><th>Max</th></tr>");
            while(rs.next()){
                out.println("<tr>");
                out.println("<td class=\"text-center\">"+rs.getString("category")+"</td>");
                out.println("<td class=\"text-center\">"+rs.getInt("customerid")+"</td>");
                out.println("<td class=\"text-center\">"+rs.getInt("max")+"</td>");
                out.println("</tr>");
            }
            out.println("</table></div>");

        }
        catch(SQLException e){
            e.printStackTrace();
        }

        out.println(pgb.getBootstrapScripts() + "</body></html>");
    }
}
