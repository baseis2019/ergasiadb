package servlets;

import helperpackage.DbUtil;
import helperpackage.PageBuilder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet("/QuerySix")
public class QuerySix extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private Connection connection = null;
    PageBuilder pgb = new PageBuilder();

    public void init() throws ServletException{
        try {
            connection = DbUtil.getConnection();
        } catch(Exception e) {
            throw new ServletException(e.toString());
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println(pgb.getDefaultHead("First Query"));
        out.println("<body><div align=\"center\">");

        try{
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM (SELECT companyname, COUNT(*) as maxsale FROM (transactions JOIN tickets USING(ticketid)) \n" +
                                                "\t\ttickets JOIN companies USING(companyid) GROUP BY companyname) AS maxsale\n" +
                                                "\tWHERE maxsale = (SELECT MAX(maxsale) FROM \n" +
                                                "\t\t(SELECT COUNT(*) as maxsale FROM (transactions JOIN tickets USING(ticketid)) \n" +
                                                "\t\ttickets JOIN companies USING(companyid) GROUP BY companyname) as maxsale);");
            out.println("<table class=\"table-dark table-hover table-bordered\"><tr><th>Company Name</th><th>Max Sale</th></tr>");
            while(rs.next()){
                out.println("<tr>");
                out.println("<td class=\"text-center\">"+rs.getString("companyname")+"</td>");
                out.println("<td class=\"text-center\">"+rs.getInt("maxsale")+"</td>");
                out.println("</tr>");
            }
            out.println("</table></div>");

        }
        catch(SQLException e){
            e.printStackTrace();
        }

        out.println(pgb.getBootstrapScripts() + "</body></html>");
    }
}
