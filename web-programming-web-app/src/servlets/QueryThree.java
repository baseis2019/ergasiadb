package servlets;

import helperpackage.DbUtil;
import helperpackage.PageBuilder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet("/QueryThree")
public class QueryThree extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private Connection connection = null;
    PageBuilder pgb = new PageBuilder();

    public void init() throws ServletException{
        try {
            connection = DbUtil.getConnection();
        } catch(Exception e) {
            throw new ServletException(e.toString());
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println(pgb.getDefaultHead("First Query"));
        out.println("<body><div align=\"center\">");

        try{
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT category, AVG(price) AS average_price FROM \n" +
                    "\t\t(transactions JOIN tickets USING(ticketid)) JOIN customers USING(customerid) \n" +
                    "\tWHERE age BETWEEN 16 AND 44 \n" +
                    "GROUP BY category;");
            out.println("<table class=\"table-dark table-hover table-bordered\"><tr><th>Category</th><th>Average Price</th></tr>");
            while(rs.next()){
                out.println("<tr>");
                out.println("<td class=\"text-center\">"+rs.getString("category")+"</td>");
                out.println("<td class=\"text-center\">"+rs.getDouble("average_price")+"</td>");
                out.println("</tr>");
            }
            out.println("</table></div>");

        }
        catch(SQLException e){
            e.printStackTrace();
        }

        out.println(pgb.getBootstrapScripts() + "</body></html>");
    }
}
